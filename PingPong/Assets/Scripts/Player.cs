﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Player : NetworkBehaviour
{

    [SerializeField] private float speed = 10f;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private SpriteRenderer sr;

    Dictionary<KeyCode, Color> colors = new Dictionary<KeyCode, Color>(){
    {KeyCode.Alpha1, Color.white},
    {KeyCode.Alpha2, Color.red},
    {KeyCode.Alpha3, Color.green},
    {KeyCode.Alpha4, Color.gray},
    {KeyCode.Alpha5, Color.blue},
    {KeyCode.Alpha6, Color.yellow}
};

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
        if (!isLocalPlayer)
            return;

        rb.velocity = new Vector2(0, Input.GetAxisRaw("Vertical")) * speed * Time.fixedDeltaTime;
    }

    private void Update()
    {
        if (!isLocalPlayer)
            return;

        foreach(var keyCode in colors.Keys)
        {
            if (Input.GetKeyDown(keyCode))
                CmdChangeColor(colors[keyCode]);
        }
    }

    // Puede enviarse cualquier variable sincronizada y hasta 60 por script
    // El hook es el nombre de la función que se llama cuando la variable
    // cambia, si no lo necesitamos, podemos llamar simplemente
    // [SyncVar]
    [SyncVar(hook = nameof(SetColor))]
    Color playerColor = Color.white;

    void SetColor(Color oldColor, Color newColor)
    {
        sr.color = newColor;
    }

    [Command]
    public void CmdChangeColor(Color newColor)
    {
        playerColor = newColor;
    }

}
