﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

public class ManagerNetworkPong : NetworkManager
{
     public Transform leftRacketSpawn;
     public Transform rightRacketSpawn;
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] public TMP_Text scoreText;

    private Ball ball;

    //Se llama cada que se instancia uno de los personajes.
    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        //base.OnServerAddPlayer(conn);
        Transform startPosition = numPlayers == 0 ? leftRacketSpawn : rightRacketSpawn;
        GameObject player = Instantiate(playerPrefab, startPosition.position, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, player); //al jugador le doy al autoridad de esta conexión
        if(numPlayers == 1)
        {
            var tempBall  = Instantiate(ballPrefab, Vector3.zero, Quaternion.identity);
            NetworkServer.Spawn(tempBall);
            ball = tempBall.GetComponent<Ball>();
        }
    }

    //Cuando alguien se desconecte
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        if(ball != null)
        {
            ball.textScore.text = "0 - 0";
            NetworkServer.Destroy(ball.gameObject);
        }
        base.OnServerDisconnect(conn);
    }


}
