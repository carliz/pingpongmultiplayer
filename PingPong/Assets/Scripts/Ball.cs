﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

public class Ball : NetworkBehaviour
{
    [SerializeField] private float speed = 30;
    [SerializeField] private int leftScore;
    [SerializeField] private int rightScore;

    ManagerNetworkPong networkManager;

    private Rigidbody2D rb;
    public TMP_Text textScore;
    public TMP_Text winnerScore_Text;

    public override void OnStartServer ()
    {
        //rb.simulated = true;
        //rb.velocity = Vector2.left * speed;
        textScore = FindObjectOfType<TMP_Text>();

    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        StartCoroutine(StartBall(5));
        networkManager = FindObjectOfType<ManagerNetworkPong>();
        winnerScore_Text = GameObject.Find("WinnerPlayerText").GetComponent<TMP_Text>();
        winnerScore_Text.gameObject.SetActive(false);

    }

    private IEnumerator StartBall(float delay)
    {
        rb.simulated = false;
        rb.velocity = Vector2.zero;
        transform.position = Vector2.zero;
        yield return new WaitForSeconds(2f);
        rb.simulated = true;
        float direction = Random.Range(0f, 1f) > 0.5f ? 1 : -1;
        rb.velocity = Vector2.right * speed * direction;
    }

    float HitFactor(Vector2 ballPosition, Vector2  racketPosition, float racketHeight)
    {
        return (ballPosition.y - racketPosition.y) / racketHeight;
    }

    [ServerCallback] //Solamente se ejecutan en el server
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.GetComponent<Player>())
        {
            rb.velocity = Vector2.Reflect(rb.velocity, collision.contacts[0].normal).normalized * speed;
            float y = HitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);
            float x = collision.relativeVelocity.x > 0 ? 1 : -1;
            Vector2 dir = new Vector2(x, y).normalized;
            rb.velocity = dir * speed;
        }
    }

    [ServerCallback]
    private void Update()
    {
        int delay = 0;

        if (transform.position.x > networkManager.rightRacketSpawn.position.x)
        {
            leftScore++;
            StartCoroutine(StartBall(delay));
            RpcChangeScore(leftScore, rightScore);
        }

        if (transform.position.x < networkManager.leftRacketSpawn.position.x)
        {
            rightScore++;
            StartCoroutine(StartBall(delay));
            RpcChangeScore(leftScore, rightScore);
        }

        if (transform.position.x > networkManager.rightRacketSpawn.position.x)
        {
            leftScore++;

            // Codigo nuevo
            if (leftScore >= 5)
            {
                RpcWinState("Izquierdo");
                delay = 5;
                leftScore = 0;
                rightScore = 0;
            }
            StartCoroutine(StartBall(delay));
            RpcChangeScore(leftScore, rightScore);
        }
    }

    //para que el cliente vea el cambio del texto
    [ClientRpc] 
    public void RpcChangeScore(int leftScore, int rightScore)
    {
        textScore.text = $"{leftScore} - {rightScore}";
    }

    [ClientRpc]
    public void RpcWinState(string winner)
    {
        winnerScore_Text.gameObject.SetActive(true);
        winnerScore_Text.text = $"Ha ganado el jugador del lado <b>{winner}</b>";
    }

    [ClientRpc]
    public void RpcDisableWinState()
    {
        winnerScore_Text.gameObject.SetActive(false);
    }
}
